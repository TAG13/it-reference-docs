# IT Reference Docs

IT Refence Docs is a collection of helpful documents for quick referencing. Currently this project is built around what I personally find useful for my particular job and projects, but I hope others find it useful as well. Individuals are welcome to contribute to it.

About me:
I was recently hired for an IT Help Desk role at a medium sized company. While comfortable working with computers, I did not have any formal background in IT before this job. As I'm growing my skills at work, tinkering with hobby projects, and studying for IT certifications, I've sometimes wished I had a handy guide to reference for various things, like common port numbers or useful Windows cmd line commands. Most things I found were either much more in-depth than I needed or not in a format I found convenient.
